# coding: utf-8
import logging
from outbox import Outbox, Email
from api import Issues, Redmine, TimeEntries

try:
    import settings
except ImportError:
    msg = u"configuration error"
    raise Exception(msg)


class Fcdev(Issues, TimeEntries, Redmine):
    def __init__(self, **kwargs):
        self.project = kwargs.pop('project')
        super(Fcdev, self).__init__(**kwargs)
        self.__init_with_time_entry_file_creation__()

    def __init_with_time_entry_file_creation__(self):
        self.time_entry_file = settings.TIME_ENTRY_FILE
        open(self.time_entry_file, 'a').close()

    def _get_time_entry(self):
        try:
            time_entry = open(self.time_entry_file, 'r').readline().strip() or 0
        except IOError, e:
            self.__init_with_time_entry_file_creation__()
            time_entry = 0
        return int(time_entry)

    def _set_time_entry(self, value):
        try:
            open(self.time_entry_file, 'w').write(str(value))
        except IOError, e:
            self.__init_with_time_entry_file_creation__()
            self._set_time_entry(value)

    def _del_time_entry(self):
        raise NotImplementedError

    time_entry = property(_get_time_entry, _set_time_entry, _del_time_entry, "last time entry")

    @property
    def has_new_entries(self):
        last_entry_id = self._get_time_entries(limit=1)[0]['id']
        if self.time_entry and self.time_entry == last_entry_id:
            return False
        return True

    def get_new_time_entries(self, **kwargs):
        offset, limit = 0, 100
        last_entry_id = self._get_time_entries(limit=1, **kwargs)[0]['id']
        logging.info('the last time_entry id %s' % last_entry_id)
        if not self.has_new_entries:
            logging.info('There is no new time_entries')

            return None
        elif self.has_new_entries and not self.time_entry:
            first_shot = limit * 20
            logging.info('Seems to be a first start, getting %s last time_entries' % first_shot)
            time_entries = self._get_time_entries(limit=first_shot, **kwargs)
            self.time_entry = time_entries[0]['id']

            return time_entries
        else:
            time_entries = self._get_time_entries(**kwargs)
            while self.time_entry < last_entry_id:
                offset += limit
                new_time_entries = self._get_time_entries(limit=limit, offset=offset, **kwargs)
                last_entry_id = new_time_entries[0]['id']
                time_entries += new_time_entries
            num = 0
            new_time_entries = [entry for num, entry in enumerate(time_entries) if entry['id'] > self.time_entry]
            logging.info('we have %s new time entries' % num)
            self.time_entry = new_time_entries[0]['id']

            return new_time_entries

    def _get_time_entries(self, offset=0, limit=25, **kwargs):
        time_entries, total_count = self.get_page('time_entries', offset=offset, limit=limit,
                                                  project=self.project, **kwargs)
        return time_entries


fcdev = Fcdev(url=settings.REDMINE_URL, key=settings.REDMINE_API_KEY, project=settings.PROJECT)


def send_mail(context):
    msg = u"""
    <p><strong>Превышено залогированное время <a href="%(url)s/issues/%(issue_id)s">%(issue_id)s</a></strong></p>
    <p>Время выполения превышено!</p>
    <p>На операции %(activities)s
    <i>было потрачено %(spent_hours)s ч. (оценка %(estimated_hours)s ч.)</p>
    <p><small>всего по тикету %(issue_spent_hours)s</small></i></p>
    """ % context
    subject = u"Превышено время #%(issue_id)s %(project)s - %(tracker)s - %(ticket_subject)s" % context
    recipients = list(set(context['additional_emails'] + [context['assigned_to']]))
    logging.info('Sending mail to %s' % str(recipients).strip('[\']'))
    outbox = Outbox(username=settings.EMAIL_HOST_USER,
                    password=settings.EMAIL_HOST_PASSWORD,
                    server=settings.EMAIL_HOST,
                    port=settings.EMAIL_PORT,
                    mode=settings.EMAIL_USE_TLS)
    outbox.send(Email(subject=subject, html_body=msg, recipients=recipients))


def get_additional_emails_for_project(project_id):
    """ getting additional emails for a given project id from settings
    :param project_id: project id
    """
    emails = set(settings.RECIPIENTS_ALL)
    map(lambda addr: emails.add(*addr),
        [addr for project, addr in settings.RECIPIENTS_PER_PROJECT.iteritems()
         if project == fcdev.get_project(project_id)['identifier'] or project == project_id])
    return list(emails)


def run():
    issues_watched = set({})
    new_time_entries = fcdev.get_new_time_entries() or []
    for entry in new_time_entries:
        issue_id = entry['issue']['id']
        issue = fcdev.get_issue(issue_id)['issue']
        project_name=issue.get('project')['name']
        activities = settings.ACTIVITIES
        issue_spent_hours = issue.get('spent_hours', 0)
        estimated_hours = issue.get('estimated_hours', None)
        if not estimated_hours or issue_id in issues_watched: continue
        if activities:
            activities_named = map(lambda activity_id: fcdev.get_activity_name(activity_id), activities)
            spent_hours = fcdev.get_spent_hours_for_issue(issue_id, activities=activities)
        else:
            activities_named = [u'Вся активность']
            spent_hours = issue_spent_hours
        gap = float(settings.GAP + 100) / 100
        logging.info("%-8s Ticket %-6s Spent_Hours: %-4s [on %s activity(ies)] (%-14s) Estimated_Hours: %-4s" % (
            project_name, issue_id, spent_hours, str(activities).strip('[]') or 'all',
            issue_spent_hours, estimated_hours))
        if estimated_hours \
            and spent_hours / gap > estimated_hours \
            and not fcdev.get_alert_in(issue_id):
            user_email = fcdev.get_user_email(issue.get('assigned_to')['id'])
            additional_emails = get_additional_emails_for_project(issue.get('project')['id'])
            context = dict(
                url=fcdev.api._url(),
                issue_id=issue_id,
                assigned_to=user_email,
                additional_emails=additional_emails,
                estimated_hours=round(estimated_hours, 2),
                issue_spent_hours=round(issue_spent_hours, 2),
                spent_hours=round(spent_hours, 2),
                activities=', '.join(activities_named),
                project=issue.get('project')['name'],
                ticket_subject=issue.get('subject'),
                tracker=issue.get('tracker')['name']
            )
            logging.info('%-8s Ticket %-6s Time was exceeded' % (issue.get('project')['name'], issue_id,))
            send_mail(context)
            fcdev.set_alert_in(issue_id)
        issues_watched.add(issue_id)


if __name__ == '__main__':
    logging.basicConfig(filename=settings.LOG_FILE, level=logging.INFO,
                        format='%(asctime)s %(levelname)s: %(message)s', datefmt='%Y-%m-%d %I:%M:%S')
    run()