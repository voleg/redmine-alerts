# coding: utf-8
import json
import logging
from hammock import Hammock

try:
    import settings
except ImportError:
    msg = u"configuration error"
    raise Exception(msg)

log = logging.getLogger(__name__)


class Redmine(object):
    def __init__(self, url=None, key=None):
        self.api = Hammock(url, headers={'Content-Type': 'application/json', 'X-Redmine-API-Key': key}, )

    def get_page(self, page_name, page_id=None, offset=0, limit=100, project=None, **kwargs):
        """
        Get page method returns one or several pages applying filters
        :param page_name: name of the page (first part of url) for example 'issues' or 'time_entries'
        :param page_id: the ID
        :param offset: offset of json documents
        :param limit: limit of json documents
        :param project: id or identifier of project
        """
        params = {'limit': limit, 'offset': offset}
        if project != '*':
            params.update({'project_id': project})
        params.update(kwargs)
        perm_msg = "You don't have required permissions"
        if page_id:
            resp = self.api('%s/%s.json' % (page_name, page_id)).GET(params=kwargs)
            if resp.status_code == 403:
                logging.error(perm_msg)
                raise Exception(perm_msg)
            return resp.json()
        else:
            resp = self.api('%s.json' % page_name).GET(params=params)
            if resp.status_code == 403:
                logging.error(perm_msg)
                raise Exception(perm_msg)
            return resp.json()['%s' % page_name], resp.json()['total_count']


    def get_all_pages(self, page_name, **kwargs):
        """
        Returns all pages of a given page_name and other kwargs
        :param page_name:  name of the page (first part of url) for example 'issues' or 'time_entries'
        """
        offset, limit = 0, 100
        entries, total_count = self.get_page(page_name, offset=offset, limit=limit, **kwargs)
        while offset + limit < total_count:
            offset += limit
            new_entries, total_count = self.get_page(page_name, offset=offset, limit=limit, **kwargs)
            entries += new_entries
        return entries

    def get_user_email(self, user_id):
        """ Retrieves email of a give user_id
        :param user_id: int id
        """
        response = self.api('users/%s.json' % user_id).GET()
        if response.status_code == 403:
            msg = 'Admin permissions required'
            logging.error(msg)
            raise Exception(msg)
        elif response.status_code == 404:
            msg = 'No such user (%s)' % user_id
            logging.error(msg)
            raise Exception(msg)
        user = response.json()['user']
        return user['mail']

    def get_project(self, project_id):
        """
        Retrieves a project identifier by a given id
        :param project_id: int id
        :return: str identifier
        """
        return self.get_page('projects', project_id)['project']


redmine = Redmine()


class Issues(Redmine):
    def __init__(self, **kwargs):
        super(Issues, self).__init__(**kwargs)

    def get_issue(self, issue, **kwargs):
        """
        Returns an issue
        :param issue: issue id
        :return: json document
        """
        return self.get_page('issues', page_id=issue, **kwargs)

    def set_alert_in(self, issue):
        """
        Setter of a redmine custom field 'alert'
        :param issue: int issue id
        """
        logging.info('triggering redmine custom field alert id:%s' % settings.ALERT_ID)
        self.api('issues/%s.json' % issue).PUT(data=json.dumps({
           'issue': {"custom_fields": [{"value": "1", "id": settings.ALERT_ID, "name": "alert"}]}}))

    def get_alert_in(self, issue):
        """
        Getter of a redmine custom field 'alert'
        :param issue: int issue id
        :return: True or False
        """
        issue = self.get_issue(issue)['issue']
        value = [entry['value'] for entry in issue['custom_fields'] if entry['id'] == settings.ALERT_ID] or 0
        if isinstance(value, list):
            return int(value[0])
        else:
            return int(value)


class TimeEntries(Redmine):
    def __init__(self, **kwargs):
        super(TimeEntries, self).__init__(**kwargs)

    def get_time_entries_for_issue(self, issue, **kwargs):
        """
        Retrieves redmine time entries for a given issue
        :param issue: issue id
        :return: json document
        """
        kwargs.update({'issue_id': issue})

        return self.get_all_pages('time_entries', **kwargs)

    def get_activity_name(self, activity_id, **kwargs):
        """
        Retrieves a name of activity
        :param activity_id:
        :param kwargs:
        :return: unicode string
        """
        time_entry_activities = self.get_page('enumerations',
                                              page_id='time_entry_activities', **kwargs)['time_entry_activities']
        name = [entry['name'] for entry in time_entry_activities if int(entry['id']) == int(activity_id)]
        return name[0]

    def get_spent_hours_for_issue(self, issue, activities=None, **kwargs):
        """
        Count a spent hours for issue
        :param activities: iterable of activity (development, testing, management ...)
        :param issue: issue id
        :return: float time
        """
        time_entries, time = self.get_time_entries_for_issue(issue, **kwargs), 0
        if activities:
            time = sum([entry['hours'] for entry in time_entries
                        if int(entry['activity']['id']) in [int(a) for a in activities]])
        else:
            time = sum([entry['hours'] for entry in time_entries])
        return time
