# coding: utf-8
import os

# Redmine URL and API key
REDMINE_URL = 'https://example.com'
REDMINE_API_KEY = '8dd06961b1f0697bd40018e918331a079fae73e6'

# project(ID or identifier) '*' for all projects
PROJECT = 'fut2'

# managers email, every message goes to him
RECIPIENTS_ALL = ['project_manager@example.com', 'superuser@example.com']
RECIPIENTS_PER_PROJECT = {
    'project1': ['customer@example.com', 'customer_tech@example.com'],
    '25':       ['customer@example.com']
}

# a list of needed activities to count spent time on issue
# we really need only some of them
# if nothing selected spent_time from issue well be used
ACTIVITIES = [
     13,     # development
    # 19      # testing
]

# You may consider storing some variables in environment
GAP = 10 # % the percentage of time exceeded
EMAIL_HOST = 'smtp.example.com'
EMAIL_HOST_PASSWORD = 'password'
EMAIL_HOST_USER = 'user'
EMAIL_PORT = 25
EMAIL_USE_TLS = None

# Create Redmine boolen custom-field for issue
# to mark the tasks for which emails has already been sent
ALERT_ID = 12 # ID of boolen custom-field

# script uses a home dir of a curent user or /tmp to store a state file
HOME_DIR = os.path.expanduser('~')
if not os.path.exists(HOME_DIR): HOME_DIR = '/tmp'
TIME_ENTRY_FILE = os.path.join(HOME_DIR, 'redmine-alerts-time-entry-id.txt')
LOG_FILE = os.path.join(HOME_DIR,'redmine-alerts.log')